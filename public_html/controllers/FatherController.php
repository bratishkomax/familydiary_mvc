<?php

include_once ROOT_DIR . '/models/User.php';
include_once ROOT_DIR . '/traits/LogoutTrait.php';

class FatherController
{
    use LogoutTrait;
    public function distributeTask()
    {
        if($_SESSION['member'] != 'Father') header("Location:/404");
        self::logout();

        $user_notice = '';

        $tasks = User::getAllTasks();
        $names = User::getAllNames();

        if(!$tasks || !$names) $user_notice = 'No loaded tasks or database error';

        require_once ROOT_DIR . '/views/distribute_tasks.php';

        if (isset($_POST['jstr'])) {
            $data = json_decode($_POST['jstr'], true);
            if(!User::distribute_tasks($data)) $user_notice = "Could not upadate 'tasks' table";
            else $user_notice = "updating success";
            header('Location: ' . $_SERVER['PHP_SELF']);
        }

        return true;
    }
}