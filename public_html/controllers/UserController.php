<?php

include_once ROOT_DIR . '/models/User.php';
include_once ROOT_DIR . '/traits/LogoutTrait.php';

class UserController
{
    use LogoutTrait;

    public function register()
    {
        $user_notice = '';

        if (isset($_POST['submit'])) {

            $name = htmlentities(trim($_POST['name']));
            $password = htmlentities(trim($_POST['password']));
            $member = $_POST['member'];

            $user_notice = User::register($name, $member, $password);
        }
        require_once ROOT_DIR . '/views/register.php';
        return true;
    }

    public function login()
    {
        $user_notice = '';

        if (isset($_POST['submit']))
        {
            $name = htmlentities(trim($_POST['name']));
            $password = htmlentities(trim($_POST['password']));

            $user_notice = User::login($name, $password);

            if($user_notice == "Login success") {
                header("Location:diary/" . $name);
                exit();
            }
        }
        require_once ROOT_DIR . '/views/login.php';
        return true;
    }

    public function viewTable($user_name)
    {
        if($_SESSION['user_name'] != $user_name){
            header("Location:/404");
            exit();
        }
        self::logout();

        $user_notice = '';
        $result = User::getTasks($user_name);
        if(!$result) $user_notice = "You have no task to do";

        $file_upload_notice = '';
        if(isset($_POST["upload_file"])) {
            include_once ROOT_DIR . '/controllers/MotherController.php';
            $file_upload_notice = MotherController::uploadFile();
        }

        self::markTask();

        require_once ROOT_DIR . '/views/diary.php';
        return true;
    }

    public function markTask()
    {
        $user_notice = '';
        if(isset($_POST['mark_task'])) {
            $id = $_POST['chbox_id'];
            $mark = isset($_POST['chbox'])?1:0;

            if(!User::updateTable($id, $mark)) $user_notice = "Could not upadate 'tasks' table";
            else header('Refresh: 0;URL=');
        }
    }

    public function show404()
    {
        require_once ROOT_DIR . '/views/page404.php';
        return true;
    }
}