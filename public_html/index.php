<?php
ini_set('display_errors',1);
error_reporting(E_ALL);

define('ROOT_DIR', dirname(__FILE__));
require_once(ROOT_DIR . '/components/Router.php');
require_once(ROOT_DIR . '/components/Db.php');

session_start();

$router = new Router();
$router->run();

