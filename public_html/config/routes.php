<?php
return array(

    '^login$' => 'user/login',
    '^register$' => 'user/register',
    '^diary' =>'user/viewTable',
    '^task_distribute$' => 'father/distributeTask',
    '404' => 'user/show404',
    '.+' => 'user/show404',
    '' => 'user/login',


);