btn_save = document.getElementById("btn_save_tasks");
btn_save.addEventListener("click", save);

function save(e)
{        
    var Tasks = {
        user_name:[],
        task_id:[]
    };
    
    var elems = document.querySelectorAll(".opt");
    var task_count = 0;

    for (var i = 0; i < elems.length; i++) {

        for (var j = 0; j < elems[i].options.length; j++) {

            var option = elems[i].options[j];

            if(option.selected && option.innerText != "") {

                Tasks.user_name[task_count] = option.innerText;
                Tasks.task_id[task_count] = elems[i].id;
                task_count++;
            }
        }
    }

    send_json(Tasks);
}

function send_json(Tasks)
{    
    var hr = new XMLHttpRequest();
    var url = "task_distribute";

    var json_str = JSON.stringify(Tasks);
    var data = "jstr=" + json_str;

    hr.open("POST", url, true);
    hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    hr.onreadystatechange = function() {
	    if(hr.readyState == 4 && hr.status == 200) {
		    var return_data = hr.responseText;
			document.getElementById("status").innerHTML = "updating success";
	    }
    }
    hr.send(data);
    document.getElementById("status").innerHTML = "processing...";
    //console.log (data);

}
