<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 01.08.17
 * Time: 15:04
 */

trait LogoutTrait
{
    public function logout()
    {
        if (isset($_POST['logout'])) {
            session_destroy();
            header('Location:/login');
            exit();
        }
    }
}