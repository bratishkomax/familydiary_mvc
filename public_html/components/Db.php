<?php

class Db
{
    protected static $dbh = null;
    private static $connection_error = false;

    public static function newConnection()
    {
        $params_path = ROOT_DIR . "/config/db_params.php";
        $params = include($params_path);

        try{
            self::$dbh = new PDO('mysql:host=' . $params['host'] . ';dbname=' . $params['dbname'], $params['username'], $params['passwd']);
        }
        catch (PDOException $e){
            self::$connection_error = true;
            return $e->getMessage();
            die();
        }
    }

    public static function dropConnection()
    {
        self::$dbh = null;
    }
}