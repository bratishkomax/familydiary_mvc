<?php

class Router
{
    private $routes;

    public function __construct()
    {
        $routes_path = ROOT_DIR . '/config/routes.php';
        $this->routes = include($routes_path);
    }

    private function getUri()
    {
        if(!empty($_SERVER['REQUEST_URI']))
            return trim($_SERVER['REQUEST_URI'], '/');
    }

    public function run()
    {
        $uri = $this->getUri();

        foreach ($this->routes as $uri_pattern=>$path){

            if(preg_match("~$uri_pattern~",$uri)){

                $internal_route = preg_replace("~$uri_pattern~", $path, $uri);

                $segments = explode('/', $internal_route);

                $controller_name = ucfirst(array_shift($segments)) . 'Controller';

                $action_name = array_shift($segments);

                $parameters = $segments;

                $controller_file = ROOT_DIR . '/controllers/' . $controller_name . '.php';

                if(file_exists($controller_file))
                    include_once ($controller_file);

                $ctr_obj = new $controller_name;
                $result = call_user_func_array(array($ctr_obj, $action_name), $parameters);
                if($result != NULL) break;
            }
        }
    }
}