
<html>
    <head>
        <title>Error 404</title>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="templates/css/styles.css"/>
    </head>
    <body>
        <header>
            <h1>Simple Family Diary</h1>
            <div id="errors"></div>
        </header>

            <div class="empty_body">
                <p>Page not found</p>

                    Back to <a href="../login">login form</a>
            </div>

        <footer>
            &copy;Max's Simple Family Diary</br>
            <span>All rights reserved</span>
        </footer>

    </body>

</html>