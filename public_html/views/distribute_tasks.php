<?php include ROOT_DIR . '/views/header.php'; ?>

<script src="scripts/save_distrib_tasks.js"></script>

<?php

if($user_notice != ''){
    echo "<div class='content'> $user_notice </div>";
} else {


    $i = 0;
    $arr_names = [];

    while($obj = $names->fetchObject()) {
        $arr_names[$i] = $obj->name;
        $i++;
    }

    $str_start = <<<START
        <div class="content">
            <table>
                <tr>
                    <th width="20%">Name</th>
                    <th width="80%">Task</th>
                </tr>
START;

                $str = "";

                while($obj = $tasks->fetchObject()) {

                $str_select = "";
                foreach($arr_names as $name) {
                if ($name == $obj->name) continue;
                $str_select .= "<option value=\"$name\">$name</option> " ;

                }

                $str .= <<<MAIN
                <tr>
                    <td>
                        <select name="member" class="opt" id="$obj->id">
                            <option value="$obj->name">$obj->name</option>
                            $str_select
                        </select>
                    </td>
                    <td>$obj->task</td>
                </tr>
MAIN;
                }

                $str_end = <<<END
            </table>
            <div>
                <input type="button" name="distrib" class="btns" id="btn_save_tasks" value="Save"></br>
                <p><input type="button" class="btns" value="Back" onclick="history.back()"> </p>
                <div id="status"></div>
            </div>
        </div>
    
END;
    echo $str_start . $str . $str_end;
}
    ?>

<?php include ROOT_DIR . '/views/sidebar.php'; ?>
<?php include ROOT_DIR . '/views/footer.php'; ?>

