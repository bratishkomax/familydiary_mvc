
<script src="scripts/save_distrib_tasks.js"></script>

<aside>
    <form action="" name="logout" method="post">
        <p>You entered as "<?php echo $_SESSION['user_name']; ?>"</p>

        <input type="submit" name="logout" value="Logout">
    </form>


    <p></p>

    <?php
    if ($_SESSION['member'] == "Mother"){

        echo <<<STR_UPLOAD
           <hr>Special permission:

            <form enctype="multipart/form-data" method="post">
                <p>Upload "Home tasks" file</p>
                <input type="hidden" name="MAX_FILE_SIZE" value="4096" />
                <input type="file" name="home_tasks" /><br>
                <input type="submit" name="upload_file" value="Upload" />
            </form>
             <span>$file_upload_notice</span>
STR_UPLOAD;
    }

    if ($_SESSION['member'] == "Father"){
        echo <<<STR_DISTR
            <hr><p>Special permission:</p>

            <form action="../task_distribute" name="distribute" method="post">
                <input type="submit" name="distribute_tasks" value="Distribute Tasks">
            </form>      
STR_DISTR;
    }

    ?>
</aside>