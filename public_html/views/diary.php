<?php include ROOT_DIR . '/views/header.php'; ?>

<?php
    if($user_notice != ''){
        echo "<div class='content'> $user_notice </div>";
    } else {

        $str_start = <<<START
        <div class="content">
            <table>
                <tr>
                    <th width="70%">Task</th>
                    <th width="15%">Mark done</th>
                    <th width="15%">Save</th>
                </tr>
START;

        $str = "";

        while ($obj = $result->fetchObject()) {

            $str_chk_box = "";
            if ($obj->mark) $str_chk_box = "checked";

            $str .= <<<MAIN
                <tr>
                    <form action="" method="post">
                        <td>$obj->task</td>
                        <td><input type="checkbox" name="chbox" value ="$obj->id" $str_chk_box></td>
                        <td><input type="submit" name="mark_task" value="Save"></td>
                        <input type="hidden" name="chbox_id" value="$obj->id">
                    </form>
                </tr>
MAIN;
        }

            $str_end = <<<END
                </table>
            </div>
END;
            echo $str_start . $str . $str_end;
    }
?>

<?php include ROOT_DIR . '/views/sidebar.php'; ?>
<?php include ROOT_DIR . '/views/footer.php'; ?>
