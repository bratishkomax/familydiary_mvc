<?php include ROOT_DIR . '/views/header.php';?>

<div class="empty_body">
    <h2>Login or go to <a href="register">registration</a> form</h2>

    <form action="" name="loginForm" method="post">
        <input type="text" name="name" placeholder="enter your name" required><br>
        <input type="password" name="password" placeholder="enter your password" required><br>
        <input type="submit" name="submit" value="Login">
    </form>
    <div id="login_state"><?php echo $user_notice;?></div>
</div>

<?php include ROOT_DIR . '/views/footer.php'; ?>
