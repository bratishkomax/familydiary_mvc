<?php include ROOT_DIR . '/views/header.php'; ?>

<div class="empty_body">
    <h2>Registration form</h2>

    <form action="" name="regForm" method="post">
        <p> Input your name<br>
            <input type="text" name="name" placeholder="name" required>
        </p>
        <p>
            Input your password<br>
            <input type="password" name="password" placeholder="password" required>
        </p>
        <p>
            You are:<br>
            <select name="member">
                <option value="Mother">Mother</option>
                <option value="Father">Father</option>
                <option value="Child">Child</option>
            </select>
        <p>

            <input type="submit" name="submit" value="Register">
            or go back to <a href="../login">login form</a>
    </form>
    <div id="register_state"><?php echo $user_notice; ?></div>
</div>


<?php include ROOT_DIR . '/views/footer.php'; ?>
