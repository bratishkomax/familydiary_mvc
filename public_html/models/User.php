<?php

class User extends Db
{
    public static function register($name, $member, $password)
    {
        if(empty($name) || empty($password) || empty($member)) return "Fill all the fields";

        if (self::userExist($name)) return "User already exist";

        $password_hash = password_hash($password, PASSWORD_BCRYPT);

        if(!self::tableExist('users')) {
            if(self::createNewTable('users') == false) return "Error while creating table";
        }

        self::newConnection();

        $sth = self::$dbh->prepare("INSERT INTO users VALUES( ?, ?, ?, ?)");
        $state = $sth->execute(array(null, $name, $member, $password_hash));

        self::dropConnection();

        if(!$state) return "Could not register user";

        return "Register success";
    }

    public static function login($name, $password)
    {
        if (empty($name)) return "Input your name";
        if (empty($password)) return "Input your password";

        if(!self::tableExist('users')) return "No registered users";

        self::newConnection();

        $sth = self::$dbh->prepare("SELECT * FROM users WHERE name = :name");
        $sth->bindParam(':name', $name);
        $result = $sth->execute();

        self::dropConnection();

        $row = $sth->fetch();

        if(!$row) return "Wrong name";
        else {
            if(password_verify($password, $row['password'])) {
                $_SESSION['user_name'] = $row['name'];
                $_SESSION['member'] = $row['member'];
                return "Login success";
            }
            else return "Wrong password";
        }
    }

    private static function userExist($name)
    {
        self::newConnection();

        $sth = self::$dbh->prepare("SELECT name FROM users WHERE name = :name");
        $sth->bindParam(':name', $name);
        $result = $sth->execute();

        self::dropConnection();

        if ($sth->fetch()) return true;

        return false;
    }

    protected static function createNewTable($table)
    {
        self::newConnection();

        switch($table){

            case 'users':
                $result = self::$dbh->query("CREATE TABLE IF NOT EXISTS users(
                    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                    name VARCHAR(20) NOT NULL,
                    member  VARCHAR(10) NOT NULL,
                    password  VARCHAR(255) NOT NULL)
                    ENGINE=INNODB");
                break;

            case 'tasks':
                $result = self::$dbh->query("CREATE TABLE IF NOT EXISTS tasks(
                    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                    user_id INT,
                    task  VARCHAR(200) NOT NULL,
                    mark BOOLEAN NOT NULL,
                    INDEX (user_id),
                    FOREIGN KEY (user_id) REFERENCES users(id)
                    ON UPDATE CASCADE ON DELETE SET NULL)
                    ENGINE=INNODB");
                break;

            default:
                $result = 0;
                break;
        }
        self::dropConnection();

        return $result;
    }

    protected static function tableExist($table)
    {
        self::newConnection();

        $sth = self::$dbh->prepare("SHOW TABLES LIKE :table");
        $sth->bindParam(':table', $table);
        $result = $sth->execute();

        self::dropConnection();

        if($sth->rowCount() > 0) return true;
        else return false;
    }

    public static function getTasks($name)
    {
        if (!self::tableExist('tasks')) return false;

        self::newConnection();

        $sth = self::$dbh->prepare("SELECT tasks.* FROM tasks INNER JOIN users ON tasks.user_id = users.id WHERE users.name = :name");
        $sth->bindParam(':name', $name);
        $result = $sth->execute();

        self::dropConnection();

        if($sth->columnCount() > 0) return $sth;
        else return false;
    }


    public static function updateTable($id, $mark)
    {
        self::newConnection();

        $sth = self::$dbh->prepare("UPDATE tasks SET mark = :mark WHERE id = :id");
        $sth->bindParam(':mark', $mark);
        $sth->bindParam(':id',$id);
        $result = $sth->execute();

        self::dropConnection();

        return $result;
    }

    public static function getAllTasks()
    {
        if (!User::tableExist('tasks') || !User::tableExist('users')) return false;

        self::newConnection();

        $result = self::$dbh->query("SELECT users.name, tasks.task, tasks.id FROM tasks LEFT JOIN users ON users.id = tasks.user_id");

        self::dropConnection();

        return $result;
    }

    public static function getAllNames()
    {
        if (!User::tableExist('users')) return false;

        self::newConnection();

        $result = self::$dbh->query("SELECT name FROM users");

        self::dropConnection();

        return $result;
    }

    public static function distribute_tasks($data)
    {
        self::newConnection();

        $sth = self::$dbh->prepare("UPDATE tasks, users SET tasks.user_id = users.id WHERE users.name = :name AND tasks.id = :id");

        for ($i = 0; $i < count($data['user_name']); $i++) {
            $name = $data['user_name'][$i];
            $id = $data['task_id'][$i];

            $sth->bindParam(':name', $name);
            $sth->bindParam(':id', $id);
            $result = $sth->execute();
        }

        self::dropConnection();

        return $result;
    }

    public static function loadTasks($file_name)
    {
        if(!self::tableExist('tasks')) self::createNewTable('tasks');

        if($_FILES[$file_name]["error"] == UPLOAD_ERR_OK) {
            $lines = file($_FILES[$file_name]["tmp_name"]);
        }
        else {
            return "Error upload file";
        }

        self::newConnection();

        self::$dbh->query("DELETE FROM tasks");

        $sth = self::$dbh->prepare("INSERT INTO tasks (task) VALUES (:line)");
        foreach($lines as $line) {

            $sth->bindParam(':line',$line);
            $sth->execute();
        }

        self::dropConnection();

        return "File uploaded successful";
    }

}